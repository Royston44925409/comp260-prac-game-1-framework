﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public enum Player
    {
        Player1,
        Player2,
    }

    private BeeSpawner beeSpawner;

    void Start()
    {
        beeSpawner = FindObjectOfType<BeeSpawner>();
    }
    public Player choosePlayer;
    Vector2 direction;

    public float maxSpeed = 5.0f;
    public float acceleration = 3.0f; // in metres/second/second
    private float speed = 0.0f; // in metres/second
    public float brake = 5.0f; // in metres/second/second
    private float maxBrake = 5.0f; 
    public float turnSpeed = 30.0f; // in degrees/second

    public float destroyRadius = 1.0f;

    void Update()
    {
       
        switch (choosePlayer)
        {
            case Player.Player1:
                float turn = Input.GetAxis("Horizontal1");
                float forwards = Input.GetAxis("Vertical1");

                // turn the car
                transform.Rotate(0, 0, turn * turnSpeed * Time.deltaTime);

              if (Input.GetButtonDown("Fire1"))
        {
            // destroy nearby bees
            beeSpawner.DestroyBees(
            transform.position, destroyRadius);
        }   

                if (forwards > 0)
                {
                    // accelerate forwards
                    speed = speed + acceleration * Time.deltaTime;
                }
                else if (forwards < 0)
                {
                    // accelerate backwards
                    speed = speed - acceleration * Time.deltaTime;
                }
                else
                {
                    // braking
                    if (speed > 0)
                    {
                        speed = speed - brake * Time.deltaTime;
                    }
                    else
                    {
                        speed = speed + brake * Time.deltaTime;
                    }
                }

                // clamp the speed
                speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
                brake = Mathf.Clamp(brake, -maxBrake, maxBrake);
                
                // compute a vector in the up direction of length speed
                Vector2 velocity = Vector2.up * speed;

                // move the object
                transform.Translate(velocity * Time.deltaTime, Space.Self);

                break;

            case Player.Player2:
                float turn2 = Input.GetAxis("Horizontal2");
                float forwards2 = Input.GetAxis("Vertical2");

                // turn the car
                transform.Rotate(0, 0, turn2 * turnSpeed * Time.deltaTime);

                if (forwards2 > 0)
                {
                    // accelerate forwards
                    speed = speed + acceleration * Time.deltaTime;
                }
                else if (forwards2 < 0)
                {
                    // accelerate backwards
                    speed = speed - acceleration * Time.deltaTime;
                }
                else
                {
                    // braking
                    if (speed > 0)
                    {
                        speed = speed - brake * Time.deltaTime;
                    }
                    else
                    {
                        speed = speed + brake * Time.deltaTime;
                    }
                }

                // clamp the speed
                speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);
                brake = Mathf.Clamp(brake, -maxBrake, maxBrake);

                // compute a vector in the up direction of length speed
                Vector2 velocity2 = Vector2.up * speed;

                // move the object
                transform.Translate(velocity2 * Time.deltaTime, Space.Self);

                break;

        }

    }

}